<?php 

if( strtoupper($_SERVER['REQUEST_METHOD'] ==='POST' )){


    $username = htmlentities(isset($_POST['username'])) ? $_POST['username'] : null;


    $pass = htmlentities(isset($_POST['userpassword'])) ? $_POST['userpassword'] : null;


    $salt = 'XyZzy12*_';

    
    $nopass = "";

    $noname = "";

    $wrongpass = "";

    $stored_hash = '1a52e17fa899cf40fb04cfc42e6352f1';


    $md5 = hash('md5',"$salt$pass");




    if(empty($username)) {
    $noname = 'El nombre de usuario es obligatorio<br>';
    }

    if(empty($pass)) {
    $nopass =  'La constraseña es obligatoria';
    }

    if($md5!=$stored_hash && $pass!=null){

    $wrongpass =  'Contraseña incorrecta!';
    
    }elseif(isset($md5) && $md5 === $stored_hash){

    header("Location: game.php?name=".urlencode($_POST['username'])) and die();
    
    }   
}

?>





<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rock Paper Scissors / Manuel Martín</title>
</head>
<body>

    <h1>Welcome to Rock Paper Scissors</h1>
    <h4>Por favor, haz log-in.</h4>

    <form method="POST">
        <label for="username">Introduce tu nombre de usuario:</label><br>
        <input type="text" name="username"><br>
        <label for="userpassword">Introduce tu contraseña: </label><br>
        <input type="password" name="userpassword"><br>
        <input type="submit" value="Iniciar Sesión" style="margin-top:10px">
    </form>

    <p><?= isset($noname) ? $noname : "";  ?></p>
    <p><?= isset($nopass) ? $nopass : ""; ?></p>
    <p><?= isset($wrongpass) ? $wrongpass : ""; ?></p>
    
    
</body>
</html>

