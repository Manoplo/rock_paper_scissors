<?php 
require_once('functions.php');

$username = htmlentities($_GET['name']);

if(!$username){

    die('Falta el nombre del parámetro');
}

if(strtoupper($_SERVER['REQUEST_METHOD']==='POST')){

    if(isset(($_POST["logout"]))){

        header('Location: index.php') and die();
    }

    // Lógica del programa aquí 

    // Variable con la opción elegida por el jugador
    $human = $_POST["options"];

    // Opciones de la máquina
    $names = array('Piedra', 'Papel', 'Tijeras');

    // Tira un dado
    $randomoption = rand(0, 2);

    // Almacena en una variable la tirada random NOTA: He intentado usar array_rand pero por algún motivo no me funciona. 
    $computer = $names[$randomoption];

}



?>


<!--Vista-->


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rock Paper Scissors // Manuel Martín</title>
</head>
<body>

    <h1>Escoge una: PIEDRA, PAPEL O TIJERAS:</h1>
    
    <?= "<h3>Bienvenido, $username!</h3>"?>

    <form method="POST">

        <select name="options" id="options">
            <option value="Select">Select</option>
            <option value="Piedra">Piedra</option>
            <option value="Papel">Papel</option>
            <option value="Tijeras">Tijeras</option>
            <option value="Test">Test</option>
        </select>

        <input type="submit" name="play" value="Jugar">
        <input type="submit" name="logout" value="Logout">
    </form>

    <br>

    
    <textarea name="" placeholder="<?php $_SERVER['REQUEST_METHOD']==='POST' ? testing($human, $names) : ""; ?>" id="" cols="60" rows="15"><?php echo $_SERVER['REQUEST_METHOD']==='POST' ? check($human, $computer) : ""; ?></textarea>


    
</body>
</html>